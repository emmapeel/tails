# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2019-02-21 12:48+0000\n"
"PO-Revision-Date: 2019-01-19 17:20+0000\n"
"Last-Translator: \n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Install from Debian, Ubuntu, or Mint using the command line and GnuPG\"]]\n"
msgstr "[[!meta title=\"Installer depuis Debian, Ubuntu ou Mint avec la ligne de commande et GnuPG\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/expert\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/expert\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
msgid "Start in Debian, Ubuntu, or Linux Mint."
msgstr "Démarrez Debian, Ubuntu ou Linux Mint."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"verify-key\">Verify the Tails signing key</h1>\n"
msgstr "<h1 id=\"verify-key\">Vérifiez la clé de signature de Tails</h1>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If you already certified the Tails signing key with your own key, you\n"
"can skip this step and start [[downloading and verifying the USB\n"
"image|usb#download]].</p>\n"
msgstr ""
"<p>Si vous avez déjà certifié la clé de signature Tails avec votre propre clé, vous\n"
"pouvez passer cette étape et [[télécharger et vérifier l'image\n"
"USB|usb#download]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
msgid ""
"In this step, you will download and verify the *Tails signing key* which is "
"the OpenPGP key that is used to cryptographically sign the Tails USB image."
msgstr ""
"À cette étape, vous allez télécharger et vérifier la *clé de signature de "
"Tails* qui est la clé OpenPGP utilisée pour signer cryptographiquement "
"l'image USB de Tails."

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To follow these instructions you need to have your own OpenPGP\n"
"key.</p>\n"
msgstr "<p>Pour suivre ces instructions, vous aurez besoin d'une clé OpenPGP.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To learn how to create yourself an OpenPGP key, see\n"
"<a href=\"https://help.riseup.net/en/security/message-security/openpgp/gpg-keys\">Managing\n"
"OpenPGP Keys</a> by <em>Riseup</em>.</p>\n"
msgstr ""
"<p>Pour apprendre à créer une clé OpenPGP, voir\n"
"<a href=\"https://help.riseup.net/fr/security/message-security/openpgp/gpg-keys\">Gestion\n"
"des clés OpenPGP</a> par <em>Riseup</em>.</p>\n"

#. type: Plain text
msgid ""
"This verification technique uses the OpenPGP Web of Trust and the "
"certification made by official Debian developers on the Tails signing key. "
"[[Learn more about the OpenPGP Web of Trust|install/download#openpgp]]."
msgstr ""
"Cette méthode de vérification utilise la toile de confiance OpenPGP et les "
"certifications faites par des développeurs Debian officiels à la clé de "
"signature de Tails. [[En savoir plus à propos de la toile de confiance "
"OpenPGP|install/download#openpgp]]."

#. type: Bullet: '1. '
msgid ""
"Import the Tails signing key in your <span class=\"application\">GnuPG</"
"span> keyring:"
msgstr ""
"Importez la clé de signature de Tails dans votre trousseau <span class="
"\"application\">GnuPG</span> :"

#. type: Plain text
#, no-wrap
msgid ""
"       wget https://tails.boum.org/tails-signing.key\n"
"       gpg --import < tails-signing.key\n"
msgstr ""
"       wget https://tails.boum.org/tails-signing.key\n"
"       gpg --import < tails-signing.key\n"

#. type: Bullet: '1. '
msgid ""
"Install the Debian keyring. It contains the OpenPGP keys of all Debian "
"developers:"
msgstr ""
"Installez le trousseau Debian. Il contient toutes les clés OpenPGP des "
"développeurs Debian :"

#. type: Plain text
#, no-wrap
msgid "       sudo apt install debian-keyring\n"
msgstr "       sudo apt install debian-keyring\n"

#. type: Bullet: '1. '
msgid ""
"Import the OpenPGP key of [[!wikipedia Stefano_Zacchiroli]], a former Debian "
"Project Leader, from the Debian keyring into your keyring:"
msgstr ""
"Importez la clé OpenPGP de [[!wikipedia_fr Stefano_Zacchiroli]], un ancien "
"leader du projet Debian, du trousseau Debian vers le votre :"

#. type: Plain text
#, no-wrap
msgid "       gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export zack@upsilon.cc | gpg --import\n"
msgstr "       gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export zack@upsilon.cc | gpg --import\n"

#. type: Bullet: '1. '
msgid "Verify the certifications made on the Tails signing key:"
msgstr "Vérifiez les certifications faites sur la clé de signature de Tails :"

#. type: Plain text
#, no-wrap
msgid "       gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
msgstr "       gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"

#. type: Plain text
#, no-wrap
msgid "   In the output of this command, look for the following line:\n"
msgstr "   Dans la sortie de cette commande, cherchez la ligne suivante :\n"

#. type: Plain text
#, no-wrap
msgid "       sig! 0x9C31503C6D866396 2015-02-03  Stefano Zacchiroli <zack@upsilon.cc>\n"
msgstr "       sig! 0x9C31503C6D866396 2015-02-03  Stefano Zacchiroli <zack@upsilon.cc>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   Here, <code>sig!</code>, with an exclamation mark, means that Stefano\n"
"   Zacchiroli verified and certified the Tails signing key with his key.\n"
msgstr ""
"   Ici, <code>sig!</code>, avec un point d'exclamation, signifie que Stefano\n"
"   Zacchiroli à vérifié et certifié la clé de signature de Tails avec sa clé.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   It is also possible to verify the certifications made by other\n"
"   people. Their name and email address appear in the list of\n"
"   certification if you have their key in your keyring.\n"
msgstr ""
"   Il est aussi possible de vérifier les certifications faites par d'autres\n"
"   personnes. Leur nom et adresse email apparaissent dans la liste des\n"
"   certifications si vous avez leur clé dans votre trousseau.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"caution\">\n"
"   <p>If the verification of the certification failed, then you might\n"
"   have downloaded a malicious version of the Tails signing key or our\n"
"   instructions might be outdated.\n"
"   Please [[get in touch with us|support/talk]].</p>\n"
"   </div>\n"
msgstr ""
"   <div class=\"caution\">\n"
"   <p>Si la validation de la certification échoue, il se peut que vous\n"
"   ayez téléchargé une version malicieuse de la clé de signature de Tails ou que nos\n"
"   instructions soient dépassées.\n"
"   Merci d'[[entrer en contact avec nous|support/talk]].</p>\n"
"   </div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"tip\">\n"
"   <p>The line `175 signatures not checked due to missing keys` or similar\n"
"   refers to the certifications (also called *signatures*) made by other public\n"
"   keys that are not in your keyring. This is not a problem.</p>\n"
"   </div>\n"
msgstr ""
"   <div class=\"tip\">\n"
"   <p>La ligne `175 signatures non vérifiées à cause de clefs manquantes` ou similaire\n"
"   fait référence aux autres certifications (aussi appelées *signatures*) faites par d'autres clés publiques\n"
"   qui ne sont pas dans votre trousseau. Ce n'est pas un problème.</p>\n"
"   </div>\n"

#. type: Bullet: '1. '
msgid "Certify the Tails signing key with your own key:"
msgstr "Certifiez la clé de signature de Tails avec votre clé :"

#. type: Plain text
#, no-wrap
msgid ""
"   a. To make a non-exportable certification that will never be shared\n"
"      with others:\n"
msgstr ""
"   a. Pour faire une signature non-exportable qui ne sera jamais partagée avec\n"
"      d'autres personnes :\n"

#. type: Plain text
#, no-wrap
msgid "          gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
msgstr "          gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"

#. type: Plain text
#, no-wrap
msgid ""
"   b. To make an exportable certification of the Tails signing\n"
"      key and publish it on the public key servers:\n"
msgstr ""
"   b. Pour faire une certification exportable de la clé de signature de Tails\n"
"      et la publier sur les serveurs de clés publiques :\n"

#. type: Plain text
#, no-wrap
msgid ""
"          gpg --sign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
"          gpg --send-keys A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
msgstr ""
"          gpg --sign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
"          gpg --send-keys A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"

#. type: Plain text
#, no-wrap
msgid ""
"      Doing so allows people who verified\n"
"      your key to verify your certification and, as a consequence, build\n"
"      more trust in the Tails signing key.\n"
msgstr ""
"      Faire cela permet aux personnes ayant vérifié\n"
"      votre clé de vérifier votre certification et, par conséquent, avoir\n"
"      plus confiance en la clé de signature de Tails.\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"download\"></a>\n"
msgstr "<a id=\"download\"></a>\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"download-verify\">Download and verify the USB image</h1>\n"
msgstr "<h1 id=\"download-verify\">Télécharger et vérifier l'image USB</h1>\n"

#. type: Plain text
msgid ""
"In this step, you will download the latest Tails USB image and verify it "
"using the Tails signing key."
msgstr ""
"À cette étape, vous allez télécharger la dernière image USB de Tails et la "
"vérifier avec la clé de signature de Tails."

#. type: Bullet: '1. '
msgid "Download the USB image:"
msgstr "Télécharger l'image USB :"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre\">wget --continue [[!inline pages=\"inc/stable_amd64_img_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">wget --continue [[!inline pages=\"inc/stable_amd64_img_url\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Bullet: '1. '
msgid "Download the signature of the USB image:"
msgstr "Télécharger la signature de l'image USB :"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre\">wget [[!inline pages=\"inc/stable_amd64_img_sig_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">wget [[!inline pages=\"inc/stable_amd64_img_sig_url\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Bullet: '1. '
msgid "Verify that the USB image is signed by the Tails signing key:"
msgstr "Vérifier que l'image USB est signée par la clé de signature de Tails :"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_verify\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_verify\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Plain text
#, no-wrap
msgid "   The output of this command should be the following:\n"
msgstr "   La sortie de cette commande devrait être la suivante :\n"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Plain text
#, no-wrap
msgid "   Verify in this output that:\n"
msgstr "   Vérifiez que :\n"

#. type: Bullet: '     - '
msgid "The date of the signature is the same."
msgstr "La date de signature est la même."

#. type: Bullet: '     - '
msgid ""
"The signature is marked as <code>Good signature</code> since you certified "
"the Tails signing key with your own key."
msgstr ""
"La signature est marquée comme <code>Good signature</code> puisque vous avez "
"certifié la clé de signature de Tails avec votre clé."

#. type: Plain text
#, no-wrap
msgid "<a id=\"dd\"></a>\n"
msgstr "<a id=\"dd\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Install Tails using <span class=\"command\">dd</span>\n"
msgstr "Installer Tails avec <span class=\"command\">dd</span>\n"

#. type: Bullet: '1. '
msgid ""
"Make sure that the USB stick on which you want to install Tails is unplugged."
msgstr ""
"Assurez-vous que la clé USB sur laquelle vous voulez installer Tails est "
"débranchée."

#. type: Bullet: '1. '
msgid "Execute the following command:"
msgstr "Exécutez la commande suivante :"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command\">ls -1 /dev/sd?</p>\n"
msgstr "   <p class=\"pre command\">ls -1 /dev/sd?</p>\n"

#. type: Plain text
#, no-wrap
msgid "   It returns a list of the storage devices on the system. For example:\n"
msgstr "   Elle retourne une liste des périphériques de stockage du système. Par exemple :\n"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command-output\">/dev/sda</p>\n"
msgstr "   <p class=\"pre command-output\">/dev/sda</p>\n"

#. type: Bullet: '1. '
msgid "Plug in the USB stick on which you want to install Tails."
msgstr "Branchez la clé USB sur laquelle vous voulez installer Tails."

#. type: Plain text
#, no-wrap
msgid "   <div class=\"caution\"><p>All the data on this USB stick will be lost.</p></div>\n"
msgstr "   <div class=\"caution\"><p>Toutes les données sur cette clé USB seront perdues.</p></div>\n"

#. type: Bullet: '1. '
msgid "Execute again the same command:"
msgstr "Exécutez à nouveau la même commande :"

#. type: Plain text
#, no-wrap
msgid "   Your USB stick appears as a new device in the list.\n"
msgstr "   Votre clé USB apparaît comme nouveau périphérique dans la liste.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <p class=\"pre command-output\">/dev/sda\n"
"   /dev/sdb</p>\n"
msgstr ""
"   <p class=\"pre command-output\">/dev/sda\n"
"   /dev/sdb</p>\n"

#. type: Bullet: '1. '
msgid "Take note of the *device name* of your USB stick."
msgstr "Prenez note du *nom de périphérique* de votre clé USB."

#. type: Plain text
#, no-wrap
msgid ""
"   In this example, the device name of the USB stick is\n"
"   <span class=\"code\">/dev/sdb</span>. Yours might be different.\n"
msgstr ""
"   Dans cet exemple, le nom de périphérique de votre clé USB est\n"
"   <span class=\"code\">/dev/sdb</span>. Le vôtre peut-être différent.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"caution\">\n"
"   <p>If you are unsure about the device name, you should stop proceeding or\n"
"   <strong>you risk overwriting any hard disk on the system</strong>.</p>\n"
"   </div>\n"
msgstr ""
"   <div class=\"caution\">\n"
"   <p>Si vous n'avez pas la certitude d'avoir le bon nom de périphérique, vous devriez ne pas poursuivre ou\n"
"   <strong>vous risquez d'écraser les données d'un disque dur de votre système</strong>.</p>\n"
"   </div>\n"

#. type: Bullet: '1. '
msgid ""
"Execute the following commands to copy the USB image that you downloaded "
"earlier to the USB stick."
msgstr ""
"Exécutez les commandes suivantes pour copier sur la clé USB l'image USB que "
"vous avez téléchargée précédemment."

#. type: Plain text
#, no-wrap
msgid "   Replace:\n"
msgstr "   Remplacez:\n"

#. type: Bullet: '   - '
msgid ""
"<span class=\"command-placeholder\">tails.img</span> with the path to the "
"USB image"
msgstr ""
"<span class=\"command-placeholder\">tails.img</span> par le chemin vers "
"l'image USB"

#. type: Bullet: '   - '
msgid ""
"<span class=\"command-placeholder\">device</span> with the device name found "
"in step 5"
msgstr ""
"<span class=\"command-placeholder\">périphérique</span> par le nom de "
"périphérique trouvé à l'étape 5"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command\">dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">device</span> bs=16M && sync</p>\n"
msgstr "   <p class=\"pre command\">dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">périphérique</span> bs=16M && sync</p>\n"

#. type: Plain text
#, no-wrap
msgid "   You should get something like this:\n"
msgstr "   Vous devriez obtenir quelque chose qui ressemble à :\n"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command-example\">dd if=/home/user/tails-amd64-3.12.img of=/dev/sdb bs=16M && sync</p>\n"
msgstr "   <p class=\"pre command-example\">dd if=/home/user/tails-amd64-3.12.img of=/dev/sdb bs=16M && sync</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   If no error message is returned, Tails is being copied on the USB\n"
"   stick. The copy takes some time, generally a few minutes.\n"
msgstr ""
"   Si aucun message d'erreur n'est retourné, Tails est en train d'être copié sur la clé\n"
"   USB. La copie prend du temps, généralement quelques minutes.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"note\">\n"
"   <p>If you get a <span class=\"guilabel\">Permission denied</span> error, try\n"
"   adding <code>sudo</code> at the beginning of the command:</p>\n"
msgstr ""
"   <div class=\"note\">\n"
"   <p>Si vous obtenez une erreur <span class=\"guilabel\">Permission denied</span>, essayez\n"
"   d'ajouter <code>sudo</code> au début de la commande :</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <p class=\"pre command\">sudo dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">device</span> bs=16M && sync</p>\n"
"   </div>\n"
msgstr ""
"   <p class=\"pre command\">sudo dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">périphérique</span> bs=16M && sync</p>\n"
"   </div>\n"

#. type: Plain text
#, no-wrap
msgid "   The installation is complete after the command prompt reappears.\n"
msgstr "   L'installation est terminée lorsque l'invite de commande réapparaît.\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_first_time.inline.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/create_persistence.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/create_persistence.inline.fr\" raw=\"yes\" sort=\"age\"]]\n"

#, fuzzy
#~| msgid "<div class=\"note\">\n"
#~ msgid "   <div class=\"note\">\n"
#~ msgstr "<div class=\"note\">\n"

#, fuzzy
#~| msgid "</div>\n"
#~ msgid "   </div>\n"
#~ msgstr "</div>\n"

#~ msgid ""
#~ "[[!inline pages=\"install/inc/steps/debian_requirements.inline\" raw=\"yes"
#~ "\" sort=\"age\"]]\n"
#~ msgstr ""
#~ "[[!inline pages=\"install/inc/steps/debian_requirements.inline.fr\" raw="
#~ "\"yes\" sort=\"age\"]]\n"

#~ msgid ""
#~ "In this step, you will install <span class=\"application\">Tails\n"
#~ "Installer</span>, a program designed specifically for installing Tails.\n"
#~ msgstr ""
#~ "À cette étape, vous allez installer l'<span class=\"application"
#~ "\">Installeur de\n"
#~ "Tails</span>, un programme conçu spécifiquement pour installer Tails.\n"

#~ msgid "If you are running:"
#~ msgstr "Si vous utilisez :"

#, fuzzy
#~| msgid ""
#~| "   a. Debian, execute the following command to add the\n"
#~| "   backports repository to your system:\n"
#~ msgid ""
#~ "   a. Debian, execute the following commands to add the\n"
#~ "   backports repository to your system:\n"
#~ msgstr ""
#~ "   a. Debian, exécutez la commande suivante pour ajouter le\n"
#~ "   dépôt backports à votre système :\n"

#~ msgid ""
#~ "         BACKPORTS='deb http://http.debian.net/debian/ stretch-backports "
#~ "main'\n"
#~ "         echo $BACKPORTS | sudo tee /etc/apt/sources.list.d/stretch-"
#~ "backports.list && echo \"OK\"\n"
#~ msgstr ""
#~ "         BACKPORTS='deb http://http.debian.net/debian/ stretch-backports "
#~ "main'\n"
#~ "         echo $BACKPORTS | sudo tee /etc/apt/sources.list.d/stretch-"
#~ "backports.list && echo \"OK\"\n"

#~ msgid ""
#~ "   b. Ubuntu or Linux Mint, execute the following commands to add the\n"
#~ "   *universe* repository and our PPA to your system:\n"
#~ msgstr ""
#~ "   b. Ubuntu ou Linux Mint, exécutez les commandes suivantes pour ajouter "
#~ "le dépôt\n"
#~ "   *universe* et notre PPA à votre système :\n"

#~ msgid ""
#~ "         sudo add-apt-repository universe\n"
#~ "         sudo add-apt-repository ppa:tails-team/tails-installer\n"
#~ msgstr ""
#~ "         sudo add-apt-repository universe\n"
#~ "         sudo add-apt-repository ppa:tails-team/tails-installer\n"

#~ msgid "Update your lists of packages:"
#~ msgstr "Mettez à jour votre liste de paquets :"

#~ msgid "       sudo apt update\n"
#~ msgstr "       sudo apt update\n"

#~ msgid "Install the <span class=\"code\">tails-installer</span> package:"
#~ msgstr "Installez le paquet <span class=\"code\">tails-installer</span> :"

#~ msgid "       sudo apt install tails-installer\n"
#~ msgstr "       sudo apt install tails-installer\n"

#~ msgid ""
#~ "[[!inline pages=\"install/inc/steps/clone.inline\" raw=\"yes\" sort=\"age"
#~ "\"]]\n"
#~ msgstr ""
#~ "[[!inline pages=\"install/inc/steps/clone.inline.fr\" raw=\"yes\" sort="
#~ "\"age\"]]\n"
