# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2018-03-12 14:36+0100\n"
"PO-Revision-Date: 2018-04-06 12:50+0200\n"
"Last-Translator: Tails translators\n"
"Language-Team: Tails Translators <tails@boum.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.3\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta title=\"Privacy for anyone anywhere\"]] [[!meta google-site-"
"verification=\"aGp--gO0AaDbtxshkcWaS0jY8WkOjYwUuBqRWzWHy6o\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Tails is a [[!wikipedia desc=\"live operating system\" Live_USB]] that you "
"can start on almost any computer from a USB stick or a DVD."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"It aims at preserving your <strong>privacy</strong> and <strong>anonymity</"
"strong>, and helps you to:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<strong>use the Internet anonymously</strong> and <strong>circumvent "
"censorship</strong>;"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"all connections to the Internet are forced to go through [[the Tor network|"
"https://www.torproject.org/]];"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<strong>leave no trace</strong> on the computer you are using unless you ask "
"it explicitly;"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<strong>use state-of-the-art cryptographic tools</strong> to encrypt your "
"files, emails and instant messaging."
msgstr ""

#. type: Content of: <div><p>
msgid "[[Learn more about Tails.|about]]"
msgstr ""

#. type: Content of: <div><h1>
msgid "News"
msgstr ""

#. type: Content of: <div>
msgid ""
"[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
"(currentlang() or news/report_2* or news/test_*)\" show=\"2\" feeds=\"no\" "
"archive=\"yes\" sort=\"-meta(date) age -path\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid "See [[News]] for more."
msgstr ""

#. type: Content of: <div><h1>
msgid "Security"
msgstr ""

#. type: Content of: <div>
msgid ""
"[[!inline pages=\"page(security/*) and !security/audits and !security/audits."
"* and !security/audits/* and !security/*/* and !security/discussion and "
"(currentlang() or security/Numerous_security_holes_in_*)\" show=\"2\" feeds="
"\"no\" archive=\"yes\" sort=\"-meta(date) age -path\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid "See [[Security]] for more."
msgstr ""

#. type: Content of: <div><div>
msgid ""
"<a href=\"https://www.debian.org/\" class=\"noicon\">[[!img lib/debian.png "
"link=\"no\"]]</a>"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Tails is built upon <a href=\"https://www.debian.org/\">Debian</a>."
msgstr ""

#. type: Content of: <div><div>
msgid "[[!img lib/free-software.png link=\"doc/about/license\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Tails is [[Free Software|doc/about/license]]."
msgstr ""

#. type: Content of: <div><div>
msgid ""
"<a href=\"https://www.torproject.org/\" class=\"noicon\">[[!img lib/tor.png "
"link=\"no\"]]</a>"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Tails sends its traffic through <a href=\"https://torproject.org/\">Tor</a>."
msgstr ""

#.  #debian-fs-tor
#. type: Content of: outside any tag (error?)
msgid "<span class=\"clearfix\"></span>"
msgstr ""

#. type: Content of: <div><h1>
msgid "Awards"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Tails has received the Mozilla Open Source Support Award (2016), the Access "
"Innovation Prize (2014), the APC FLOSS prize (2014) and the OpenITP award "
"(2013)."
msgstr ""

#. type: Content of: <div><h1>
msgid "Partners & Grants"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Tails receives grants, corporate donations, and substantial donations from "
"individuals."
msgstr ""

#. type: Content of: <div><p>
msgid "[[Meet our partners|partners]] or [[become a partner|partners/become]]!"
msgstr ""
