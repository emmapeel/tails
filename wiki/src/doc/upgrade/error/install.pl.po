# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-01-30 12:40+0000\n"
"PO-Revision-Date: 2018-07-02 04:55+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Error while installing the upgrade\"]]\n"
msgstr ""

#. type: Plain text
msgid "The upgrade could not be installed."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
msgid ""
"Your Tails USB stick needs to be repaired and might be unable to restart."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
msgid "This is probably caused by a software error in Tails Upgrader."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"1. Please [[report an error|support]] and include in your\n"
"report:\n"
msgstr ""

#. type: Bullet: '   - '
msgid "the debugging information that appears in the error message"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   - the output of the five following commands, run in a\n"
"    [[<span class=\\\"application\\\">Terminal</span>|first_steps/introduction_to_gnome_and_the_tails_desktop#terminal]]:\n"
"    <pre>\n"
"        ls -l /lib/live/mount/medium/live\n"
"        cat /lib/live/mount/medium/live/Tails.module\n"
"        mount\n"
"        df -h\n"
"        free -m\n"
"    </pre>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"2. To repair your Tails USB stick, [[manually upgrade|first_steps/upgrade#manual]]\n"
"it using Tails Installer from another Tails USB stick.\n"
msgstr ""

#. type: Plain text
msgid ""
"If the manual upgrade fails as well, please [[report another error|"
"first_steps/bug_reporting]]."
msgstr ""
