# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-11-12 18:11+0000\n"
"PO-Revision-Date: 2018-10-27 07:01+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Create & configure the persistent volume\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/persistence.caution.pl\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=3]]\n"
msgstr "[[!toc levels=3]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"To start the persistent volume assistant, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Tails</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Configure persistent volume</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"The error message <span class=\"emphasis\">Error, Persistence partition is not\n"
"unlocked.</span> means that the persistent volume was not enabled from\n"
"<span class=\"application\">Tails Greeter</span>. So you can not configure it\n"
"but you can delete it and create a new one.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
msgid ""
"When run for the first time, or after [[deleting the persistent volume|"
"delete]], the assistant proposes to create a new persistent volume on the "
"USB stick. Refer to our [[installation instructions|install/clone#create-"
"persistence]] for more guidance on creating the persistent volume."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Persistence features\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<strong>Restart Tails to apply the changes</strong> after selecting or\n"
"deselecting one or several features.\n"
msgstr ""

#. type: Plain text
msgid ""
"Only features that are listed here can currently be made persistent. Some "
"other features have been asked and accepted, but are waiting to be "
"implemented: browser extensions, [[!tails_ticket 7148 desc=\"wallpaper\"]], "
"[[!tails_ticket 7246 desc=\"default sound card\"]], [[!tails_ticket 5979 "
"desc=\"mouse and touchpad settings\"]], etc. See the [[corresponding tickets|"
"https://redmine.tails.boum.org/code/projects/tails/issues?query_id=122]] for "
"more details."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"bug\" id=\"deselect\">\n"
msgstr "<div class=\"bug\" id=\"deselect\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If you deselect a feature that used to be activated, it will be\n"
"deactivated after restarting Tails but the\n"
"[[corresponding files|doc/first_steps/persistence/copy#feature_files]]\n"
"will remain on the persistent volume.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>To delete the files corresponding to a feature:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<ol>\n"
"  <li>\n"
"    Start Tails and set an\n"
"    [[administration password|doc/first_steps/startup_options/administration_password]].\n"
"  </li>\n"
"  <li>\n"
"    Choose\n"
"    <span class=\"menuchoice\">\n"
"      <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"      <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
"      <span class=\"guimenuitem\">Root Terminal</span>\n"
"    </span>\n"
"    to open a terminal with administration rights.\n"
"  </li>\n"
"  <li>\n"
"    Execute the <span class=\"code\">nautilus</span> command to open the file\n"
"    browser with administration rights.\n"
"  </li>\n"
"  <li>\n"
"    In the file browser, navigate to <span class=\"filename\">\n"
"    /live/persistence/TailsData_unlocked</span>.\n"
"  </li>\n"
"  <li>\n"
"    Delete the [[folder corresponding to the feature|doc/first_steps/persistence/copy#feature_files]].\n"
"  </li>\n"
"</ol>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"personal_data\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"icon\">\n"
msgstr "<div class=\"icon\">\n"

#. type: Plain text
#, no-wrap
msgid "[[!img stock_folder.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Personal Data</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When this feature is activated, you can save your personal files and working\n"
"documents in the <span class=\"filename\">Persistent</span> folder.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To open the <span class=\"filename\">Persistent</span> folder, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Places</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Persistent</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"browser_bookmarks\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img user-bookmarks.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Browser Bookmarks</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When this feature is activated, changes to the bookmarks in\n"
"[[<span class=\"application\">Tor Browser</span>|doc/anonymous_internet/Tor_Browser]]\n"
"are saved in the persistent volume. This does not apply to the\n"
"[[<span class=\"application\">Unsafe Browser</span>|doc/anonymous_internet/unsafe_browser]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"network_connections\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img network-manager.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Network Connections</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"When this feature is activated, the [[configuration of the network devices "
"and connections|doc/anonymous_internet/networkmanager]] is saved in the "
"persistent volume."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"additional_software\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img additional-software.png link=no]]\n"
msgstr "[[!img additional-software.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Additional Software</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"When this feature is enabled, a list of [[additional software|doc/"
"first_steps/additional_software]] of your choice is automatically installed "
"every time you start Tails."
msgstr ""

#. type: Plain text
msgid ""
"The corresponding software packages are stored in the persistent volume. "
"They are automatically upgraded for security after a network connection is "
"established."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>The packages included in Tails are carefully tested for security.\n"
"Installing additional packages might break the security built in Tails,\n"
"so [[be careful with what you install|additional_software#warning]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"printers\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img printer.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Printers</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"When this feature is activated, the [[configuration of the printers|doc/"
"sensitive_documents/printing_and_scanning]] is saved in the persistent "
"volume."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"thunderbird\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img thunderbird.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Thunderbird</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When this feature is activated, the configuration and emails stored\n"
"by the\n"
"[[<span class=\"application\">Thunderbird</span> email client|doc/anonymous_internet/thunderbird]]\n"
"are saved in the persistent volume.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"gnupg\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img seahorse-key.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>GnuPG</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"When this feature is activated, the OpenPGP keys that you create or import "
"are saved in the persistent volume."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"If you manually edit or overwrite the\n"
"<span class=\"filename\">~/.gnupg/gpg.conf</span> configuration file\n"
"you may lessen your anonymity,\n"
"weaken the encryption defaults or render GnuPG unusable.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"bitcoin\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img electrum.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Bitcoin Client</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"When this feature is activated, the bitcoin wallet and preferences of the "
"[[*Electrum* bitcoin client|anonymous_internet/electrum]] are saved in the "
"persistent volume."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"pidgin\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img pidgin.png link=no]]\n"
msgstr "[[!img pidgin.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Pidgin</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When this feature is activated, all the configuration files of the\n"
"[[<span class=\"application\">Pidgin</span> Internet messenger|doc/anonymous_internet/pidgin]]\n"
"are saved in the persistent volume:\n"
msgstr ""

#. type: Bullet: '  - '
msgid "The configuration of your accounts, buddies and chats."
msgstr ""

#. type: Bullet: '  - '
msgid "Your OTR encryption keys and keyring."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"The content of the discussions is not saved unless you configure <span class="
"\"application\">Pidgin</span> to do so."
msgstr ""

#. type: Plain text
msgid ""
"All the configuration options are available from the graphical interface. "
"There is no need to manually edit or overwrite the configuration files."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"ssh_client\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img seahorse-key-ssh.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>SSH Client</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"When this feature is activated, all the files related to the secure-shell "
"client are saved in the persistent volume:"
msgstr ""

#. type: Bullet: '  - '
msgid "The SSH keys that you create or import"
msgstr ""

#. type: Bullet: '  - '
msgid "The public keys of the hosts you connect to"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"The SSH configuration file in <span class=\"filename\">~/.ssh/config</span>"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"If you manually edit the <span class=\"filename\">~/.ssh/config</span>\n"
"configuration file, make sure not to overwrite the\n"
"default configuration from the\n"
"<span class=\"filename\">/etc/ssh/ssh_config</span> file. Otherwise, you may weaken the\n"
"encryption defaults or render SSH unusable.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"dotfiles\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img preferences-desktop.png link=no]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"text\"><h2>Dotfiles</h2></div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When this feature is activated, all the files in the <span\n"
"class=\"filename\">/live/persistence/TailsData_unlocked/dotfiles</span> folder\n"
"are linked in the <span class=\"filename\">Home</span> folder. Files in\n"
"subfolders of <span class=\"filename\">dotfiles</span> are also linked\n"
"in the corresponding subfolder of your <span class=\"filename\">Home\n"
"</span> folder.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"For example, having the following files in <span\n"
"class=\"filename\">/live/persistence/TailsData_unlocked/dotfiles</span>:\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    /live/persistence/TailsData_unlocked/dotfiles\n"
"    ├── file_a\n"
"    ├── folder\n"
"    │   ├── file_b\n"
"    │   └── subfolder\n"
"    │       └── file_c\n"
"    └── emptyfolder\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "Produces the following result in <span class=\"filename\">/home/amnesia</span>:\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    /home/amnesia\n"
"    ├── file_a → /live/persistence/TailsData_unlocked/dotfiles/file_a\n"
"    └── folder\n"
"        ├── file_b → /live/persistence/TailsData_unlocked/dotfiles/folder/file_b\n"
"        └── subfolder\n"
"            └── file_c → /live/persistence/TailsData_unlocked/dotfiles/folder/subfolder/file_c\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"This option is useful if you want to make some specific files\n"
"persistent, but not the folders they are stored in. A fine example are\n"
"the so called \"dotfiles\" (and hence the name of this feature), the\n"
"hidden configuration files in the root of your home directory, like\n"
"<span class=\"filename\">~/.gitconfig</span> and <span\n"
"class=\"filename\">~/.bashrc</span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"As you can see in the previous example, empty folders are ignored. This feature\n"
"only links files, and not folders, from the persistent volume into the <span\n"
"class=\"filename\">Home</span> folder.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"displays\"></a>\n"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Save the configuration of your displays"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"If you have more than one display (for example, two monitors or a\n"
"projector), you can save the configuration of your displays using the\n"
"<span class=\"guilabel\">Dotfiles</span> feature.\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Activate the <span class=\"guilabel\">Dotfiles</span> feature and restart "
"Tails."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Choose <span class=\"menuchoice\"> <span class=\"guimenu\">System Tools</"
"span>&nbsp;▸ <span class=\"guisubmenu\">Settings</span>&nbsp;▸ <span class="
"\"guimenuitem\">Displays</span></span>."
msgstr ""

#. type: Bullet: '1. '
msgid "Configure your displays."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Open <span class=\"filename\">/live/persistence/TailsData_unlocked/dotfiles</"
"span> in <span class=\"application\">Files</span>."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Choose <span class=\"menuchoice\"> <span class=\"guimenu\">[[!img lib/open-"
"menu.png alt=\"Menu\" class=symbolic link=\"no\"]]</span>&nbsp;▸ <span class="
"\"guisubmenu\">Show Hidden Files</span>."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Create a folder called <span class=\"filename\">.config</span> (<span class="
"\"filename\">config</span> preceded by a dot)."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Copy the <span class=\"filename\">.config/monitors.xml</span> file from your "
"<span class=\"filename\">Home</span> folder to <span class=\"filename\">/"
"live/persistence/TailsData_unlocked/dotfiles/.config</span>."
msgstr ""
