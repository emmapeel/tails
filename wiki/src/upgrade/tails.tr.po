# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-02-22 19:05+0000\n"
"PO-Revision-Date: 2018-10-30 07:43+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Manually upgrade inside Tails (or Linux)\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/upgrade-tails\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"hidden-step-1\"></div>\n"
msgstr "<div class=\"hidden-step-1\"></div>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img install/inc/infography/os-tails.png link=\"no\" alt=\"\"]]</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p class=\"start\">Start in Tails or Linux.</p>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Install an intermediary Tails\n"
msgstr ""

#. type: Plain text
msgid ""
"In this step, you will install an intermediary Tails using the Tails USB "
"image that you downloaded earlier."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>The persistent storage of your Tails USB stick will not be\n"
"copied.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!inline pages=\"install/inc/steps/install_with_gnome_disks.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/install_final.inline.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_first_time.inline.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!inline pages=\"install/inc/steps/clone.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/install_final.inline.tr\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "<div class=\"caution\">\n"
#~ msgstr "<div class=\"caution\">\n"

#~ msgid "   </div>\n"
#~ msgstr "   </div>\n"

#~ msgid ""
#~ "   [[!inline pages=\"news/version_3.5/manual_upgrade.inline\" raw=\"yes\" "
#~ "sort=\"age\"]]\n"
#~ msgstr ""
#~ "   [[!inline pages=\"news/version_3.5/manual_upgrade.inline.tr\" raw=\"yes"
#~ "\" sort=\"age\"]]\n"
