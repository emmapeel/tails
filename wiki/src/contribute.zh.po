# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-03-20 12:53+0000\n"
"PO-Revision-Date: 2018-07-02 05:52+0000\n"
"Last-Translator: qf <selinaf1917@yahoo.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Contributing to Tails\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"There are many ways you can contribute to Tails. No effort is too small and "
"whatever you bring to this community will be appreciated.  So read on to "
"find out how you can make a difference in Tails."
msgstr ""
"你有很多种向Tails做贡献的途径。 任何尝试都不会微不足道，且无论你给社区带来什"
"么都会被认可。 所以请继续读下去，看看有没有你给Tails带来不一样的地方。"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"contribute-roles-1\">\n"
"<h2>Share your experience</h2>\n"
"<div class=\"contribute-role\" id=\"user\">\n"
msgstr ""
"<div class=\"contribute-roles-1\">\n"
"<h2>分享你的经验</h2>\n"
"<div class=\"contribute-role\" id=\"user\">\n"

#. type: Plain text
#, no-wrap
msgid "  [[!img user.png link=no]]\n"
msgstr "  [[!img user.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>Every user can help others or provide developers with useful information.</p>\n"
"  <ul>\n"
"    <li>[[Report bugs|doc/first_steps/bug_reporting]]</li>\n"
"    <li>[[Test experimental ISO images|contribute/how/testing]]</li>\n"
"    <li>[[Provide input to developers|contribute/how/input]]</li>\n"
"    <li>[[Help other Tails users|contribute/how/help]]</li>\n"
"  </ul>\n"
"</div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"contribute-roles-1\">\n"
"<h2>Donate</h2>\n"
"<div class=\"contribute-role\" id=\"donate\">\n"
msgstr ""
"<div class=\"contribute-roles-1\">\n"
"<h2>捐款</h2>\n"
"<div class=\"contribute-role\" id=\"donate\">\n"

#. type: Plain text
#, no-wrap
msgid "  [[!img donate.png link=no]]\n"
msgstr "  [[!img donate.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>Donating speeds up the development of Tails.</p>\n"
"  <ul>\n"
"    <li><a href=\"https://tails.boum.org/donate/?r=contribute\">Make a donation</a></li>\n"
"  </ul>\n"
"</div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"contribute-roles-3\">\n"
"<h2>Contribute your language skills</h2>\n"
"<div class=\"contribute-role\" id=\"content-writer\">\n"
"  <h3>Writer</h3>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img writer.png link=no]]\n"
msgstr "  [[!img writer.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>Good writers can make Tails accessible to more people.</p>\n"
"  <ul>\n"
"    <li>[[Improve documentation|contribute/how/documentation]]</li>\n"
"    <li>[[Write press releases|contribute/how/promote]]</li>\n"
"  </ul>\n"
"</div>\n"
"<div class=\"contribute-role\" id=\"translator\">\n"
"  <h3>Translator</h3>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img translator.png link=no]]\n"
msgstr "  [[!img translator.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>Translators can allow more people around the world to use Tails.</p>\n"
"  <ul>\n"
"    <li>[[Improve Tails in your own language|contribute/how/translate]]</li>\n"
"  </ul>\n"
"</div>\n"
"<div class=\"contribute-role\" id=\"speaker\">\n"
"  <h3>Speaker</h3>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img speaker.png link=no]]\n"
msgstr "  [[!img speaker.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>Speakers can advocate Tails to all kinds of public.</p>\n"
"  <ul>\n"
"    <li>[[Talk at events|contribute/how/promote]]</li>\n"
"  </ul>\n"
"</div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"contribute-roles-3\">\n"
"<h2>Contribute your computer skills</h2>\n"
"<div class=\"contribute-role\" id=\"developer\">\n"
"  <h3>Developer or maintainer</h3>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img software_developer.png link=no]]\n"
msgstr "  [[!img software_developer.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>Software people with very diverse skills can improve Tails.</p>\n"
"  <ul>\n"
"    <li>[[Work on the source code|contribute/how/code]]</li>\n"
"    <li>[[Improve Tails by working on Debian|contribute/how/debian]]</li>\n"
"  </ul>\n"
"</div>\n"
"<div class=\"contribute-role\" id=\"sysadmin\">\n"
"  <h3>System administrator</h3>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img system_administrator.png link=no]]\n"
msgstr "  [[!img system_administrator.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>System administrators can contribute to the infrastructure behind Tails.</p>\n"
"  <ul>\n"
"    <li>[[Run a HTTP mirror|contribute/how/mirror]]</li>\n"
"    <li>[[Improve Tails infrastructure|contribute/how/sysadmin]]</li>\n"
"  </ul>\n"
"</div>\n"
"<div class=\"contribute-role\" id=\"designer\">\n"
"  <h3>Designer</h3>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img designer.png link=no]]\n"
msgstr "  [[!img designer.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>Designers can make Tails easier to use and more appealing.</p>\n"
"  <ul>\n"
"    <li>[[Improve the website|contribute/how/website]]</li>\n"
"    <li>[[Design graphics|contribute/how/graphics]]</li>\n"
"    <li>[[Improve the Tails user experience|contribute/how/user_experience]]</li>\n"
"  </ul>\n"
"</div>\n"
"</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"toc\">\n"
msgstr "<div class=\"toc\">\n"

#. type: Plain text
#, no-wrap
msgid "\t<h1>Table of contents</h1>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\t<ol>\n"
"\t\t<li class=\"L2\"><a href=\"#reference-documents\">Reference documents</a></li>\n"
"\t\t<li class=\"L2\"><a href=\"#tools\">Tools for contributors</a></li>\n"
"\t\t<li class=\"L2\"><a href=\"#mentors\">Mentors & guidance for new contributors</a></li>\n"
"\t\t<li class=\"L2\"><a href=\"#release-cycle\">Release cycle</a></li>\n"
"\t\t<li class=\"L2\"><a href=\"#upstream\">Relationship with upstream</a></li>\n"
"\t\t<li class=\"L2\"><a href=\"#collective-process\">Collective process</a></li>\n"
"\t\t<li class=\"L2\"><a href=\"#talk\">Talk with us</a></li>\n"
"\t</ol>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div> <!-- .toc -->\n"
msgstr "</div> <!-- .toc -->\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
msgid ""
"This section is only in English, because there is currently no way to "
"contribute to Tails if you do not understand English."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"reference-documents\"></a>\n"
msgstr "<a id=\"reference-documents\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Reference documents\n"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Design documents|contribute/design]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Blueprints|blueprint]] to help structuring ideas for future improvements"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Merge policy|contribute/merge_policy]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!tails_website contribute/how/promote/material/logo desc=\"Logo\"]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Personas|contribute/personas]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"tools\"></a>\n"
msgstr "<a id=\"tools\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Tools for contributors\n"
msgstr ""

#. type: Bullet: '  - '
msgid "Source code: [[Git repositories|contribute/git]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!tails_roadmap desc=\"Roadmap\"]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  - [[Redmine bug tracker|contribute/working_together/Redmine]]\n"
"    - [[Starter tasks|starter_tasks]] for new contributors\n"
"    - [Tasks](https://redmine.tails.boum.org/code/projects/tails/issues)\n"
"      can be filtered by type of work (see links in the sidebar)\n"
"  - [[Building a Tails image|contribute/build]]\n"
"    - [[Build a local copy of the website|contribute/build/website]]\n"
"    - [[Customize Tails|contribute/customize]]\n"
"    - [Nightly ISO builds](http://nightly.tails.boum.org)\n"
"  - Debian packages\n"
"    - [[APT repository|contribute/APT_repository]], to store our custom Debian packages\n"
"    - How we manage and upgrade the [[Linux kernel|contribute/Linux_kernel]].\n"
"  - [[Glossary for contributors|contribute/glossary]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"mentors\"></a>\n"
msgstr "<a id=\"mentors\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Mentors & guidance for new contributors\n"
msgstr ""

#. type: Plain text
msgid ""
"Once you have found a first [[Starter task|starter_tasks]] to work on, you "
"might need some guidance."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Here is a list of mentors who can help with\n"
"specific tasks. Feel free to talk to them if you plan to work on anything related to their\n"
"field of expertise, for example\n"
"by assigning them tickets on Redmine or <a href=\"#talk\">talking to us</a>\n"
"using the usual communication channels.\n"
msgstr ""

#. type: Bullet: '  - '
msgid "AppArmor: intrigeri, jvoisin, u"
msgstr "AppArmor: intrigeri, jvoisin, u"

#. type: Bullet: '  - '
msgid "Build system (Vagrant, Rake): anonym"
msgstr "Build system (Vagrant, Rake): anonym"

#. type: Bullet: '  - '
msgid "Debian related work: intrigeri, u"
msgstr ""

#. type: Bullet: '  - '
msgid "Documentation: BitingBird, sajolida"
msgstr ""

#. type: Bullet: '  - '
msgid "*Onion Circuits*: alan"
msgstr "*Onion Circuits*: alan"

#. type: Bullet: '  - '
msgid "*OpenPGP Applet*: nodens"
msgstr "*OpenPGP Applet*: nodens"

#. type: Bullet: '  - '
msgid "Persistence setup: intrigeri, kurono"
msgstr ""

#. type: Bullet: '  - '
msgid "Sysadmin: [[contact|contribute/how/sysadmin/#contact]]"
msgstr ""

#. type: Bullet: '  - '
msgid "*Tails Greeter*: alan, intrigeri"
msgstr "*Tails Greeter*: alan, intrigeri"

#. type: Bullet: '  - '
msgid "*Tails Installer*: alan, kurono, u"
msgstr ""

#. type: Bullet: '  - '
msgid "*Tails Upgrader*: intrigeri"
msgstr "*Tails Upgrader*: intrigeri"

#. type: Bullet: '  - '
msgid "*Tails Verification*: sajolida, anonym"
msgstr ""

#. type: Bullet: '  - '
msgid "Test suite: anonym"
msgstr "Test suite: anonym"

#. type: Bullet: '  - '
msgid "*Thunderbird* (Icedove): anonym"
msgstr "*Thunderbird* (Icedove): anonym"

#. type: Bullet: '  - '
msgid "Tor configuration, time syncing, MAC spoofing: anonym"
msgstr ""

#. type: Bullet: '  - '
msgid "*Tor Browser*: anonym"
msgstr "*Tor Browser*: anonym"

#. type: Bullet: '  - '
msgid "Usability: sajolida, tchou"
msgstr ""

#. type: Bullet: '  - '
msgid "*Unsafe Web Browser*: anonym"
msgstr ""

#. type: Bullet: '  - '
msgid "Website: sajolida"
msgstr ""

#. type: Bullet: '  - '
msgid "*WhisperBack*: alan"
msgstr "*WhisperBack*: alan"

#. type: Plain text
#, no-wrap
msgid "<a id=\"release-cycle\"></a>\n"
msgstr "<a id=\"release-cycle\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Release cycle\n"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Release schedule|contribute/release_schedule]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  - [[Release process|contribute/release_process]]\n"
"    - [[Manual test suite|contribute/release_process/test]]\n"
"    - [[Automated test suite|contribute/release_process/test/automated_tests]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"upstream\"></a>\n"
msgstr "<a id=\"upstream\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Relationship with upstream and derivatives\n"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Relationship with upstream|contribute/relationship_with_upstream]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Improve Tails by working on Debian|contribute/how/debian]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[About creating Tails derivatives|contribute/derivatives]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"collective-process\"></a>\n"
msgstr "<a id=\"collective-process\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Collective process\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Calendar|contribute/calendar]] of releases, meetings, working sessions, "
"etc."
msgstr ""

#. type: Bullet: '  - '
msgid "[[Code of conduct|contribute/working_together/code_of_conduct]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Social contract|contribute/working_together/social_contract]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Sponsorship to attend events|contribute/sponsorship_to_attend_events]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Marking a task as Starter|contribute/working_together/"
"criteria_for_starter_tasks]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[Document progress|contribute/working_together/document_progress]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  - Roles\n"
"    - [[Accounting team|contribute/working_together/roles/accounting]]\n"
"    - [[Foundations team|contribute/working_together/roles/foundations_team]]\n"
"    - [[Help desk|contribute/working_together/roles/help_desk]]\n"
"    - Sponsor deliverables:\n"
"      - [[Team manager|contribute/working_together/roles/sponsor_deliverables/team_manager]]\n"
"      - [[Worker|contribute/working_together/roles/sponsor_deliverables/worker]]\n"
"    - [[Release manager|contribute/working_together/roles/release_manager]]\n"
"    - [[Ticket gardener|contribute/working_together/roles/ticket_gardener]]\n"
"    - [[Sysadmins|contribute/working_together/roles/sysadmins]]\n"
"    - [[Technical writer|contribute/working_together/roles/technical_writer]]\n"
"    - [[Test suite maintainers|contribute/working_together/roles/test_suite]]\n"
"    - [[UX designer|contribute/working_together/roles/ux]]\n"
"    - [[Verification extension\n"
"      maintainers|contribute/working_together/roles/verification_extension]]\n"
"  - [[Reports sent to sponsors|contribute/reports]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"talk\"></a>\n"
msgstr "<a id=\"talk\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Talk with us\n"
msgstr ""

#. type: Plain text
msgid ""
"To talk to other Tails contributors, subscribe to [[the relevant mailing "
"lists|about/contact]]."
msgstr ""
