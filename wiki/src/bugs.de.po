# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2018-10-31 09:55+0000\n"
"PO-Revision-Date: 2018-10-28 08:47+0100\n"
"Last-Translator: Tails developers <amnesia@boum.org>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Bugs\"]]\n"
msgstr "[[!meta title=\"Fehler\"]]\n"

#. type: Plain text
msgid ""
"If you've found a bug in Tails, please read [[doc/first_steps/"
"bug_reporting]]."
msgstr ""
"Wenn Sie einen Fehler in Tails gefunden haben, lesen Sie bitte [[doc/"
"first_steps/bug_reporting]]."

#. type: Plain text
msgid ""
"We don't use this section anymore, see [[contribute/working_together/"
"Redmine]] instead."
msgstr ""
"Diese Seite wird nicht mehr genutzt; benutzen Sie stattdessen bitte [[!"
"tails_redmine \"\" desc=\"Redmine\"]]."
